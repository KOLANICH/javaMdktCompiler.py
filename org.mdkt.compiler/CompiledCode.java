package org.mdkt.compiler;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import javax.tools.JavaFileObject;
import javax.tools.SimpleJavaFileObject;

public class CompiledCode extends SimpleJavaFileObject {
	private ByteArrayOutputStream baos = new ByteArrayOutputStream();
	private String className;

	public CompiledCode(String className) throws Exception {
		super(new URI(className), JavaFileObject.Kind.CLASS);
		this.className = className;
	}

	public String getClassName() {
		return this.className;
	}

	@Override
	public OutputStream openOutputStream() throws IOException {
		return this.baos;
	}

	public byte[] getByteCode() {
		return this.baos.toByteArray();
	}
}
