package org.mdkt.compiler;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import javax.tools.Diagnostic;
import javax.tools.DiagnosticCollector;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.ToolProvider;
import org.mdkt.compiler.CompilationException;
import org.mdkt.compiler.CompiledCode;
import org.mdkt.compiler.DynamicClassLoader;
import org.mdkt.compiler.ExtendedStandardJavaFileManager;
import org.mdkt.compiler.SourceCode;

public class InMemoryJavaCompiler {
	private JavaCompiler javac;
	private DynamicClassLoader classLoader;
	private Iterable<String> options;
	boolean ignoreWarnings = false;
	private Map<String, SourceCode> sourceCodes = new HashMap<String, SourceCode>();

	public static InMemoryJavaCompiler newInstance() {
		return new InMemoryJavaCompiler();
	}

	private InMemoryJavaCompiler() {
		this.javac = ToolProvider.getSystemJavaCompiler();
		this.classLoader = new DynamicClassLoader(ClassLoader.getSystemClassLoader());
	}

	public InMemoryJavaCompiler useParentClassLoader(ClassLoader parent) {
		this.classLoader = new DynamicClassLoader(parent);
		return this;
	}

	public ClassLoader getClassloader() {
		return this.classLoader;
	}

	public InMemoryJavaCompiler useOptions(String... options) {
		this.options = Arrays.asList(options);
		return this;
	}

	public InMemoryJavaCompiler ignoreWarnings() {
		this.ignoreWarnings = true;
		return this;
	}

	public Map<String, Class<?>> compileAll() throws Exception {
		if (this.sourceCodes.size() == 0) {
			throw new CompilationException("No source code to compile");
		}
		Collection<SourceCode> compilationUnits = this.sourceCodes.values();
		CompiledCode[] code = new CompiledCode[compilationUnits.size()];
		Iterator<SourceCode> iter = compilationUnits.iterator();
		for (int i = 0; i < code.length; ++i) {
			code[i] = new CompiledCode(iter.next().getClassName());
		}
		DiagnosticCollector<JavaFileObject> collector = new DiagnosticCollector<JavaFileObject>();
		ExtendedStandardJavaFileManager fileManager = new ExtendedStandardJavaFileManager(this.javac.getStandardFileManager(null, null, null), this.classLoader);
		JavaCompiler.CompilationTask task = this.javac.getTask(null, fileManager, collector, this.options, null, compilationUnits);
		boolean result = task.call();
		if (!result || collector.getDiagnostics().size() > 0) {
			StringBuffer exceptionMsg = new StringBuffer();
			exceptionMsg.append("Unable to compile the source");
			boolean hasWarnings = false;
			boolean hasErrors = false;
			for (Diagnostic<? extends JavaFileObject> d : collector.getDiagnostics()) {
				switch (d.getKind()) {
					case NOTE:
					case MANDATORY_WARNING:
					case WARNING: {
						hasWarnings = true;
						break;
					}
					default: {
						hasErrors = true;
						break;
					}
				}
				exceptionMsg.append("\n").append("[kind=").append(d.getKind());
				exceptionMsg.append(", ").append("line=").append(d.getLineNumber());
				exceptionMsg.append(", ").append("message=").append(d.getMessage(Locale.US)).append("]");
			}
			if (hasWarnings && !this.ignoreWarnings || hasErrors) {
				throw new CompilationException(exceptionMsg.toString());
			}
		}
		Map<String, Class<?>> classes = new HashMap<String, Class<?>>();
		for (String className : this.sourceCodes.keySet()) {
			classes.put(className, this.classLoader.loadClass(className));
		}
		return classes;
	}

	public Class<?> compile(String className, String sourceCode) throws Exception {
		return this.addSource(className, sourceCode).compileAll().get(className);
	}

	public InMemoryJavaCompiler addSource(String className, String sourceCode) throws Exception {
		this.sourceCodes.put(className, new SourceCode(className, sourceCode));
		return this;
	}
}
