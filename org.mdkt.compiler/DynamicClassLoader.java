package org.mdkt.compiler;

import java.util.HashMap;
import java.util.Map;
import org.mdkt.compiler.CompiledCode;

public class DynamicClassLoader extends ClassLoader {
	private Map<String, CompiledCode> customCompiledCode = new HashMap<String, CompiledCode>();

	public DynamicClassLoader(ClassLoader parent) {
		super(parent);
	}

	public void addCode(CompiledCode cc) {
		this.customCompiledCode.put(cc.getName(), cc);
	}

	@Override
	protected Class<?> findClass(String name) throws ClassNotFoundException {
		CompiledCode cc = this.customCompiledCode.get(name);
		if (cc == null) {
			//return ((ClassLoader)this).findClass(name);
			return super.findClass(name);
		}
		byte[] byteCode = cc.getByteCode();
		return this.defineClass(name, byteCode, 0, byteCode.length);
	}
}
