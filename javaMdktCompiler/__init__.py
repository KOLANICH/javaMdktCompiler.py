__all__ = ("javaCompile", )
import typing
from pathlib import Path
from JAbs import SelectedJVMInitializer

mdktNS = "org.mdkt.compiler"

neededMdktCompilerClasses = [
	mdktNS + ".InMemoryJavaCompiler",
	mdktNS + ".SourceCode"
]


def getMDKTCompilerPath():
	return Path("./org.mdkt.compiler.jar")


MDKTCompilerClassPath = getMDKTCompilerPath()
ji = SelectedJVMInitializer([MDKTCompilerClassPath, Path("./antlr4-4.8-2-SNAPSHOT-complete.jar")], neededMdktCompilerClasses)


def javaCompile(filesAbstracted: typing.Iterable[typing.Union[typing.Tuple[str, str], Path]], *args, compiler=None):
	if compiler is None:
		compiler = ji.InMemoryJavaCompiler.newInstance()
	compiler.useOptions(args)

	for it in filesAbstracted:
		if isinstance(it, Path):
			fileStem = it.stem
			fileText = it.read_text(encoding="utf-8")
		else:
			fileStem, fileText = it
		compiler.addSource(fileStem, fileText)

	compiledN = compiler.compileAll()
	compiled = {}
	for k in compiledN:
		compiled[str(k)] = ji.reflClass2Class(compiledN[k])
	return compiled
