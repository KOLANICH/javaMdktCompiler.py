#!/usr/bin/env python3
import sys
from pathlib import Path
import unittest

thisDir = Path(__file__).absolute().parent
parentDir = thisDir.parent

sys.path.insert(0, str(parentDir))

from collections import OrderedDict

dict = OrderedDict

from javaMdktCompiler import javaCompile, mdktNS

compilerSources = list((parentDir / mdktNS).glob("*.java"))

class Tests(unittest.TestCase):
	def testCompile(self):
		res = javaCompile(compilerSources)
		print(res)


if __name__ == "__main__":
	unittest.main()
